package com.example.frontend.api.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public abstract class BaseController
{
	static final String baseUri = "http://localhost:8080/";
	private final HttpClient client = HttpClient.newHttpClient();
	private final ObjectMapper objectMapper = new ObjectMapper();

	<T> T request(final HttpRequest request, final Class<T> clz)
	{
		return request(request, new TypeReference<>()
		{
			@Override
			public Type getType()
			{
				return clz;
			}
		});
	}

	<T> T request(final HttpRequest request, final TypeReference<T> typeReference)
	{
		try
		{
			final HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
			if (response.statusCode() == 200)
			{
				final String responseBody = response.body();
				if (responseBody.isBlank())
				{
					return null;
				}
				if (typeReference.getType().equals(String.class))
				{
					//noinspection unchecked
					return (T) responseBody;
				}
				else
				{
					return objectMapper.readValue(responseBody, typeReference);
				}
			}
			else
			{
				throw new RuntimeException(response.body());
			}
		}
		catch (final IOException | InterruptedException e)
		{
			throw new RuntimeException(e);
		}
	}

	void request(final HttpRequest request)
	{
		try
		{
			final HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
			if (response.statusCode() != 200)
			{
				throw new RuntimeException(response.body());
			}
		}
		catch (final IOException | InterruptedException e)
		{
			throw new RuntimeException(e);
		}
	}
}
