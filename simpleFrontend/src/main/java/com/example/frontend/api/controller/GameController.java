package com.example.frontend.api.controller;

import com.example.frontend.api.entity.Game;
import com.example.frontend.api.entity.Player;
import com.fasterxml.jackson.core.type.TypeReference;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpRequest;
import java.util.List;

public class GameController extends BaseController
{
	private static final GameController INSTANCE = new GameController();

	public static GameController get()
	{
		return INSTANCE;
	}

	private GameController()
	{
		// Prevent instantiation
	}

	public Game createGame(final Player player)
	{
		if (player == null)
		{
			throw new IllegalArgumentException("player must not be null");
		}
		try
		{
			final HttpRequest request = HttpRequest.newBuilder().uri(new URI(baseUri + "game")).POST(HttpRequest.BodyPublishers.ofString(player.name())).header("Content-Type", "application/json").build();
			return request(request, Game.class);
		}
		catch (final URISyntaxException e)
		{
			throw new RuntimeException(e);
		}
	}

	public List<Game> getGames()
	{
		try
		{
			final HttpRequest request = HttpRequest.newBuilder().uri(new URI(baseUri + "game")).GET().build();
			//noinspection unchecked
			return request(request, new TypeReference<>(){});
		}
		catch (final URISyntaxException e)
		{
			throw new RuntimeException(e);
		}
	}

	public String registerThrow(final Player player, final int pins)
	{
		if (player == null)
		{
			throw new IllegalArgumentException("player must not be null");
		}
		try
		{
			final HttpRequest request = HttpRequest.newBuilder().uri(new URI(baseUri + "game/" + player.name().replace(" ", "%20"))).POST(HttpRequest.BodyPublishers.ofString(Integer.toString(pins))).header("Content-Type", "application/json").build();
			return request(request, String.class);
		}
		catch (final URISyntaxException e)
		{
			throw new RuntimeException(e);
		}
	}
}
