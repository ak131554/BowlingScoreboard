package com.example.frontend.api.controller;

import com.example.frontend.api.entity.Player;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpRequest;

public class PlayerController extends BaseController
{
	private static final PlayerController INSTANCE = new PlayerController();

	public static PlayerController get()
	{
		return INSTANCE;
	}

	private PlayerController()
	{
		// Prevent instantiation
	}

	public Player createPlayer(final String playerName)
	{
		if (playerName == null || playerName.isBlank())
		{
			throw new IllegalArgumentException("playername must not be blank");
		}
		try
		{
			final HttpRequest request = HttpRequest.newBuilder().uri(new URI(baseUri + "player")).POST(HttpRequest.BodyPublishers.ofString(playerName)).build();
			return request(request, Player.class);
		}
		catch (final URISyntaxException e)
		{
			throw new RuntimeException(e);
		}
	}
}
