package com.example.frontend.api.controller;

import com.example.frontend.api.entity.Game;
import com.example.frontend.api.entity.Player;
import com.example.frontend.api.entity.State;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpRequest;

public class FlowController extends BaseController
{
	private static final FlowController INSTANCE = new FlowController();

	public static FlowController get()
	{
		return INSTANCE;
	}

	private FlowController()
	{
		// Prevent instantiation
	}

	public String startGames()
	{
		try
		{
			final HttpRequest request = HttpRequest.newBuilder().uri(new URI(baseUri + "flow")).POST(HttpRequest.BodyPublishers.noBody()).build();
			return request(request, String.class);
		}
		catch (final URISyntaxException e)
		{
			throw new RuntimeException(e);
		}
	}

	public State getNextTurn()
	{
		try
		{
			final HttpRequest request = HttpRequest.newBuilder().uri(new URI(baseUri + "flow")).GET().build();
			return request(request, State.class);
		}
		catch (final URISyntaxException e)
		{
			throw new RuntimeException(e);
		}
	}

	public void reset()
	{
		try
		{
			final HttpRequest request = HttpRequest.newBuilder().uri(new URI(baseUri + "flow")).DELETE().build();
			request(request);
		}
		catch (final URISyntaxException e)
		{
			throw new RuntimeException(e);
		}
	}
}
