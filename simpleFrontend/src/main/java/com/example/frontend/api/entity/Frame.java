package com.example.frontend.api.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public record Frame(Integer firstThrowInFrame, Integer secondThrowInFrame, Integer thirdThrowInFrame, Integer score)
{
}
