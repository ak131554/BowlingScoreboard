package com.example.frontend.api.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public record Game(Player player, List<Frame> frames, Frame currentFrame, Integer score)
{
}
