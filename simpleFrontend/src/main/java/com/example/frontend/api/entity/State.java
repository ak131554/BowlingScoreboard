package com.example.frontend.api.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public record State(Player currentPlayer, int numberOfFrame, int numberOfThrow)
{
}
