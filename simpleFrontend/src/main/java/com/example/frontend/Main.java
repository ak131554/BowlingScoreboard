package com.example.frontend;

import com.example.frontend.api.controller.FlowController;
import com.example.frontend.api.controller.GameController;
import com.example.frontend.api.controller.PlayerController;
import com.example.frontend.api.entity.Frame;
import com.example.frontend.api.entity.Game;
import com.example.frontend.api.entity.Player;
import com.example.frontend.api.entity.State;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Comparator;
import java.util.List;

public class Main
{
	private int numberOfPlayers = 0;

	private void execute()
	{
		try (final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)))
		{
			FlowController.get().reset();
			numberOfPlayers = getNumberOfPlayers(reader);
			System.out.println(numberOfPlayers);
			createGames(reader);
			playGames(reader);
			showWinner();
		}
		catch (final IOException | NumberFormatException e)
		{
			System.out.println("Something went wrong: " + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			FlowController.get().reset();
		}
	}

	private int getNumberOfPlayers(final BufferedReader reader) throws IOException
	{
		System.out.print("Enter number of Players: ");
		return Integer.parseInt(reader.readLine());
	}

	private void createGames(final BufferedReader reader) throws IOException
	{
		for (int i = 0; i < numberOfPlayers; i++)
		{
			System.out.print("Enter name of Player_" + (i + 1) + ": ");
			final String name = reader.readLine();
			final Player player = PlayerController.get().createPlayer(name);
			GameController.get().createGame(player);
		}
	}

	private void playGames(final BufferedReader reader) throws IOException
	{
		displayScoreboard();
		FlowController.get().startGames();
		for (State state = FlowController.get().getNextTurn(); state != null; state = FlowController.get().getNextTurn())
		{
			final Player player = state.currentPlayer();
			System.out.println("It's " + player.name() + "'s turn.");
			System.out.println("Frame " + state.numberOfFrame() + " / Throw " + state.numberOfThrow());
			boolean flag = true;
			while (flag)
			{
				flag = false;
				System.out.println("Enter number of pins: ");
				Integer pins = null;
				try
				{
					pins = Integer.parseInt(reader.readLine());
				}
				catch (final NumberFormatException e)
				{
					System.out.println("Not a number");
					flag = true;
					continue;
				}
				final String result = GameController.get().registerThrow(player, pins);
				if (!result.equals("OK"))
				{
					System.out.println(result);
					flag = true;
				}
			}
			displayScoreboard();
		}
	}

	private void showWinner()
	{
		final List<Game> games = GameController.get().getGames();
		System.out.println();
		int i=1;
		for (final Game game : games.stream().sorted(Comparator.comparing(Game::score).reversed()).toList())
		{
			final StringBuilder sb = new StringBuilder();
			switch (i)
			{
				case 1 -> sb.append("1st");
				case 2 -> sb.append("2nd");
				case 3 -> sb.append("3rd");
				default -> sb.append(i).append("th");
			}
			sb.append(" place: ").append(game.player().name()).append(" with ").append(game.score()).append(" points");
			System.out.println(sb);
			i++;
		}
	}

	private void displayScoreboard()
	{
		final List<Game> games = GameController.get().getGames();
		//noinspection OptionalGetWithoutIsPresent
		final int maxNameLength = games.stream().map(game -> game.player().name().length()).max(Integer::compare).get();
		final int scoreBoardLength = displayHeader(maxNameLength);
		games.forEach(game -> display(game, maxNameLength, scoreBoardLength));
		displayFooter(scoreBoardLength);
	}

	private int displayHeader(final int maxNameLength)
	{
		final StringBuilder sb = new StringBuilder();
		sb.append("|Player");
		if (maxNameLength > 6)
		{
			sb.append(" ".repeat(maxNameLength - 6));
		}
		for (int i = 0; i < 9; i++)
		{
			sb.append("| ").append(i + 1).append(" ");
		}
		sb.append("|  10 |");
		System.out.println("+" + "-".repeat(sb.length() - 2) + "+");
		System.out.println(sb);

		return sb.length();
	}

	private void display(final Game game, final int maxNameLength, final int scoreBoardLength)
	{
		final Player player = game.player();
		System.out.println("+" + "-".repeat(scoreBoardLength - 2) + "+");
		final StringBuilder sb1 = new StringBuilder();
		final StringBuilder sb2 = new StringBuilder();
		{
			sb1.append("|").append(player.name());
			if (player.name().length() < Math.max(maxNameLength, 6))
			{
				sb1.append(" ".repeat(Math.max(maxNameLength, 6) - player.name().length()));
			}
			sb2.append("|").append(" ".repeat(Math.max(maxNameLength, 6)));
			Integer score = null;
			for (int i = 0; i < 9; i++)
			{
				final Frame frame = game.frames().get(i);
				final Integer firstThrow = frame.firstThrowInFrame();
				final Integer secondThrow = frame.secondThrowInFrame();
				final boolean strike = firstThrow != null && firstThrow == 10;
				final boolean spare = firstThrow != null && secondThrow != null && firstThrow + secondThrow == 10;
				if (strike)
				{
					sb1.append("| |X");
				} else
				{
					sb1.append("|").append(firstThrow != null ? (firstThrow == 0 ? "-" : firstThrow) : " ").append("|").append(spare ? "/" : (secondThrow != null ? (secondThrow == 0 ? "-" : secondThrow) : " "));
				}
				sb2.append("|");
				if (frame.score() != null)
				{
					if (score == null)
					{
						score = frame.score();
					} else
					{
						score += frame.score();
					}
					if (score < 100 && score >= 10)
					{
						sb2.append(" ").append(score);
					}
					else if (score < 10)
					{
						sb2.append("  ").append(score);
					}
					else
					{
						sb2.append(score);
					}
				}
				else
				{
					sb2.append(" ".repeat(3));
				}
			}
			final Frame frame = game.frames().get(9);
			final Integer firstThrow = frame.firstThrowInFrame();
			final Integer secondThrow = frame.secondThrowInFrame();
			final Integer thirdThrow = frame.thirdThrowInFrame();
			final boolean spare = firstThrow != null && secondThrow != null && firstThrow + secondThrow == 10;
			sb1.append("|").append(firstThrow != null ? (firstThrow == 10 ? "X" : (firstThrow == 0 ? "-" : firstThrow)) : " ")
					.append("|")
					.append(secondThrow != null ? (spare ? "/" : (secondThrow == 10 ? "X" : (secondThrow == 0 ? "-" : secondThrow))) : " ")
					.append("|")
					.append(thirdThrow != null ? (thirdThrow == 10 ? "X" : (thirdThrow == 0 ? "-" : thirdThrow)) : " ")
					.append("|");
			sb2.append("|");
			if (frame.score() != null)
			{
				if (score == null)
				{
					score = frame.score();
				} else
				{
					score += frame.score();
				}
				if (score < 100 && score >= 10)
				{
					sb2.append(" ".repeat(3)).append(score);
				}
				else if (score < 10)
				{
					sb2.append(" ".repeat(4)).append(score);
				}
				else
				{
					sb2.append(" ".repeat(2)).append(score);
				}
			}
			else
			{
				sb2.append(" ".repeat(5));
			}
			sb2.append("|");
			System.out.println(sb1);
			System.out.println(sb2);
		}
	}

	private void displayFooter(final int scoreBoardLength)
	{
		System.out.println("+" + "-".repeat(scoreBoardLength - 2) + "+");
	}

	public static void main(final String[] args)
	{
		new Main().execute();
	}
}
