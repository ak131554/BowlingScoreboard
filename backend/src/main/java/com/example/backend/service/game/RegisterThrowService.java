package com.example.backend.service.game;

import com.example.backend.entity.Game;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor(access = AccessLevel.PRIVATE, onConstructor_ = @Autowired)
public class RegisterThrowService
{
    private final IndexGameService indexGameService;

    public String registerThrow(final String playerName, final int pins)
    {
        final Game game = indexGameService.findGame(playerName);
        if (game == null)
        {
            return "Game for Player " + playerName + " not found";
        }
        try
        {
            game.registerThrow(pins);
        }
        catch (final IllegalArgumentException | IllegalStateException e)
        {
            return e.getMessage();
        }
        return "OK";
    }
}
