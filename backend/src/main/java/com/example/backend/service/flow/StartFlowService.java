package com.example.backend.service.flow;

import com.example.backend.BackendApplication;
import com.example.backend.entity.DataHolder;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class StartFlowService
{
    public void startStateMachine()
    {
        final DataHolder dataHolder = BackendApplication.getDataHolder();
        if (dataHolder.getPlayers().isEmpty())
        {
            throw new IllegalStateException("No player defined");
        }
        if (!dataHolder.getGames().keySet().containsAll(dataHolder.getPlayers()))
        {
            throw new IllegalStateException("No Games created for all players");
        }
        dataHolder.getStateMachine().reset();
    }
}
