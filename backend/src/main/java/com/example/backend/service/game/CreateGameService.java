package com.example.backend.service.game;

import com.example.backend.BackendApplication;
import com.example.backend.entity.Game;
import com.example.backend.entity.Player;
import com.example.backend.service.player.CreatePlayerService;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor(access = AccessLevel.PRIVATE, onConstructor_ = @Autowired)
public class CreateGameService
{
    private CreatePlayerService createPlayerService;

    public Game createGame(final String playerName)
    {
        final Player player = createPlayerService.createPlayer(playerName);
        return BackendApplication.getDataHolder().getGames().computeIfAbsent(player, Game::new);
    }
}
