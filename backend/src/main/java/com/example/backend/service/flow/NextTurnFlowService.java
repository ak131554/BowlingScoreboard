package com.example.backend.service.flow;

import com.example.backend.BackendApplication;
import com.example.backend.state.State;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class NextTurnFlowService
{
    public State getNextState()
    {
        return BackendApplication.getDataHolder().getStateMachine().getNextState();
    }
}
