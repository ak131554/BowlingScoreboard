package com.example.backend.service.flow;

import com.example.backend.BackendApplication;
import com.example.backend.entity.DataHolder;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ResetFlowService
{
    public void reset()
    {
        final DataHolder dataHolder = BackendApplication.getDataHolder();
        dataHolder.reset();
    }
}
