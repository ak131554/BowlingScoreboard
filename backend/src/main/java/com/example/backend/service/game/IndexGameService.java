package com.example.backend.service.game;

import com.example.backend.BackendApplication;
import com.example.backend.entity.Game;
import com.example.backend.entity.Player;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class IndexGameService
{
	public List<Game> findAllGames()
	{
		//noinspection SimplifyStreamApiCallChains
		return BackendApplication.getDataHolder().getGames().entrySet().stream().map(Map.Entry::getValue).collect(Collectors.toList());
	}

	public Game findGame(final String playerName)
	{
		List<Game> allGames = findAllGames();
		return allGames.stream().filter(game -> game.getPlayer().getName().equals(playerName)).findFirst().orElse(null);
	}
}
