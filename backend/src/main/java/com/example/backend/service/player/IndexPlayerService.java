package com.example.backend.service.player;

import com.example.backend.BackendApplication;
import com.example.backend.entity.Player;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class IndexPlayerService
{
	public List<Player> findAllPlayers()
	{
		return BackendApplication.getDataHolder().getPlayers();
	}

	public Player findPlayer(final int index)
	{
		List<Player> allPlayers = findAllPlayers();
		return index >= 0 && index< allPlayers.size() ? allPlayers.get(index) : null;
	}
}
