package com.example.backend.service.player;

import com.example.backend.BackendApplication;
import com.example.backend.entity.Player;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CreatePlayerService
{
	public Player createPlayer(final String name)
	{
		final List<Player> players = BackendApplication.getDataHolder().getPlayers();
		return players.stream().filter(aPlayer -> aPlayer.getName().equals(name)).findFirst().orElseGet(() -> {
			Player newPlayer = new Player(name);
			players.add(newPlayer);
			return newPlayer;
		});
	}
}
