package com.example.backend.state;

public class StateMachine
{
    private State currentState;

    public void reset()
    {
        currentState = new State(null, 0);
    }

    public State getNextState()
    {
        currentState = currentState.getNextState();
        return currentState;
    }

    public boolean isReset()
    {
        return currentState != null && currentState.getCurrentPlayer() == null;
    }
}
