package com.example.backend.state;

import com.example.backend.BackendApplication;
import com.example.backend.entity.Frame;
import com.example.backend.entity.Game;
import com.example.backend.entity.Player;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;

import java.util.List;
import lombok.Getter;

@AllArgsConstructor
public class State
{
    @JsonProperty
    @Getter
    private Player currentPlayer;

    private int numberOfFrame;

    State getNextState()
    {
        if (currentPlayer == null)
        {
            return new State(BackendApplication.getDataHolder().getPlayers().get(0), 0);
        }
        final Game game = BackendApplication.getDataHolder().getGames().get(currentPlayer);
        final Frame frame = game.getFrames().get(numberOfFrame);
        if (frame.isThrowingFinished())
        {
            final List<Player> players = BackendApplication.getDataHolder().getPlayers();
            final Player nextPlayer = getNextPlayer();
            int nextNumberOfFrame = players.indexOf(currentPlayer) < players.indexOf(nextPlayer) ? numberOfFrame : numberOfFrame + 1;
            if (nextNumberOfFrame >= 10)
            {
                return null;
            }
            return new State(nextPlayer, nextNumberOfFrame);
        }
        else
        {
            return this;
        }
    }

    @JsonProperty
    int getNumberOfFrame()
    {
        return numberOfFrame + 1;
    }

    @JsonProperty
    int getNumberOfThrow()
    {
        final Game game = BackendApplication.getDataHolder().getGames().get(currentPlayer);
        final Frame frame = game.getCurrentFrame();
        if (frame.getFirstThrowInFrame() == null)
        {
            return 1;
        }
        else if (frame.getSecondThrowInFrame() == null)
        {
            return 2;
        }
        else
        {
            return 3;
        }
    }

    private Player getNextPlayer()
    {
        final List<Player> allPlayers = BackendApplication.getDataHolder().getPlayers();
        final int i = allPlayers.indexOf(currentPlayer);
        return allPlayers.get(i == allPlayers.size() - 1 ? 0 : i +1);
    }
}
