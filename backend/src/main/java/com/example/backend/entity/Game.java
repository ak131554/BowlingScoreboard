package com.example.backend.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@NoArgsConstructor
@Data
public class Game
{
    @NonNull
    private Player player;

    private List<Frame> frames;

    public Game(final Player player)
    {
        this.player = player;
        frames = IntStream.range(0, 10).mapToObj(Frame::new).collect(Collectors.toList());
    }

    @JsonProperty("currentFrame")
    public Frame getCurrentFrame()
    {
        return frames.stream().filter(frame -> !frame.isThrowingFinished()).findFirst().orElse(null);
    }

    public void registerThrow(final int pins)
    {
        if (pins < 0 || pins > 10)
        {
            throw new IllegalArgumentException("Number of pins mus be in range [0, 10]");
        }
        final Frame currentFrame = getCurrentFrame();
        if (currentFrame == null)
        {
            throw new IllegalStateException("Game is already over");
        }
        final List<Frame> throwingFinishedFrames = frames.stream().filter(Frame::isThrowingFinished).collect(Collectors.toList());
        Collections.reverse(throwingFinishedFrames);
        if (!currentFrame.registerThrow(pins))
        {
            throw new IllegalArgumentException("Sum of pins in current Frame would be greater than 10");
        }
        throwingFinishedFrames.forEach(frame -> frame.registerThrow(pins));
    }

    @JsonProperty("score")
    public int getScore()
    {
        return frames.stream().map(Frame::calculateScore).mapToInt(i -> i == null ? 0 : i).sum();
    }
}
