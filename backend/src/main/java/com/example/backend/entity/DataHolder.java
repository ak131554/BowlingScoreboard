package com.example.backend.entity;

import com.example.backend.state.StateMachine;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.MapSerializer;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@NoArgsConstructor
@Getter
public class DataHolder
{
    private final List<Player> players = new ArrayList<>();

    @JsonSerialize(keyUsing = MapSerializer.class)
    private final Map<Player, Game> games = new LinkedHashMap<>();

    private final StateMachine stateMachine = new StateMachine();

    public void reset()
    {
        players.clear();
        games.clear();
        stateMachine.reset();
    }
}
