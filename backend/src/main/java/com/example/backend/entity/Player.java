package com.example.backend.entity;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@NoArgsConstructor
@Data
public class Player
{
    @NonNull
    @Setter(AccessLevel.NONE)
    private String name;

    @Override
    public String toString()
    {
        return "Player{" +
                "name=" + name +
                "}";
    }
}
