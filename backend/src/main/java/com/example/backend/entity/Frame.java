package com.example.backend.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Data
public class Frame
{
    @Setter(AccessLevel.NONE)
    @JsonIgnore
    private int number;

    private Integer firstThrowInFrame;

    private Integer secondThrowInFrame;

    private Integer thirdThrowInFrame;

    @JsonIgnore
    private Integer firstThrowInNextFrame;

    @JsonIgnore
    private Integer secondThrowInNextFrame;

    public Frame(final int number)
    {
        this.number = number;
    }

    @JsonIgnore
    public boolean isThrowingFinished()
    {
        if (number == 9 && (firstThrowInFrame == null || secondThrowInFrame == null || (firstThrowInFrame + secondThrowInFrame >= 10 && thirdThrowInFrame == null)))
        {
            return false;
        }
        return firstThrowInFrame != null && (firstThrowInFrame == 10 || secondThrowInFrame != null);
    }

    @JsonIgnore
    private boolean isScoringFinished()
    {
        if (!isThrowingFinished())
        {
            return false;
        }
        if (number == 9)
        {
            return true;
        }
        if (firstThrowInFrame == 10)
        {
            return firstThrowInNextFrame != null && secondThrowInNextFrame != null;
        }
        return firstThrowInFrame + secondThrowInFrame < 10 || firstThrowInNextFrame != null;
    }

    @JsonIgnore
    boolean registerThrow(final int pins)
    {
        if (!isThrowingFinished())
        {
            if (firstThrowInFrame == null)
            {
                firstThrowInFrame = pins;
            }
            else if (secondThrowInFrame == null)
            {
                if ((number < 9 || firstThrowInFrame < 10) && firstThrowInFrame + pins > 10)
                {
                    return false;
                }
                secondThrowInFrame = pins;
            }
            else
            {
                thirdThrowInFrame = pins;
            }
        }
        else if (!isScoringFinished())
        {
            if (firstThrowInNextFrame == null)
            {
                firstThrowInNextFrame = pins;
            }
            else
            {
                secondThrowInNextFrame = pins;
            }
        }
        else
        {
            return false;
        }
        return true;
    }

    @JsonProperty("score")
    public Integer calculateScore()
    {
        if (!isScoringFinished())
        {
            return null;
        }
        // Last Frame
        if (number == 9)
        {
            return firstThrowInFrame + secondThrowInFrame + (thirdThrowInFrame == null ? 0 : thirdThrowInFrame);
        }
        else
        {
            if (firstThrowInFrame == 10)
            {
                return firstThrowInFrame + firstThrowInNextFrame + secondThrowInNextFrame;
            }
            else
            {
                final int sum = firstThrowInFrame + secondThrowInFrame;
                if (sum < 10)
                {
                    return sum;
                }
                else
                {
                    return firstThrowInNextFrame == null ? null : sum + firstThrowInNextFrame;
                }
            }
        }
    }
}
