package com.example.backend.controller;

import com.example.backend.service.flow.NextTurnFlowService;
import com.example.backend.service.flow.ResetFlowService;
import com.example.backend.service.flow.StartFlowService;
import com.example.backend.state.State;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/flow")
@AllArgsConstructor(access = AccessLevel.PRIVATE, onConstructor_ = @Autowired)
public class FlowController
{
    private final StartFlowService startFlowService;

    private final NextTurnFlowService nextTurnFlowService;

    private final ResetFlowService resetFlowService;

    @PostMapping
    @ResponseBody
    public String start()
    {
        try
        {
            startFlowService.startStateMachine();
            return "OK";
        }
        catch (final IllegalStateException e)
        {
            return e.getMessage();
        }
    }

    @DeleteMapping
    @ResponseBody
    public void reset()
    {
        resetFlowService.reset();
    }

    @GetMapping
    @ResponseBody
    public State nextTurn()
    {
        return nextTurnFlowService.getNextState();
    }
}
