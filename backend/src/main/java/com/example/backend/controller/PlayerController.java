package com.example.backend.controller;

import com.example.backend.entity.Player;
import com.example.backend.service.player.CreatePlayerService;
import com.example.backend.service.player.IndexPlayerService;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/player")
@AllArgsConstructor(access = AccessLevel.PRIVATE, onConstructor_ = @Autowired)
public class PlayerController
{
	private final CreatePlayerService createPlayerService;
	private final IndexPlayerService indexPlayerService;

	@PostMapping
	@ResponseBody
	public Player create(@RequestBody final String name)
	{
		return createPlayerService.createPlayer(name);
	}

	@GetMapping
	@ResponseBody
	public List<Player> getAll()
	{
		return indexPlayerService.findAllPlayers();
	}

	@GetMapping("/{id}")
	@ResponseBody
	public Player get(@PathVariable("id") final int id)
	{
		return indexPlayerService.findPlayer(id);
	}
}
