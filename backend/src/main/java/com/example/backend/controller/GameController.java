package com.example.backend.controller;

import com.example.backend.entity.Game;
import com.example.backend.entity.Player;
import com.example.backend.service.game.CreateGameService;
import com.example.backend.service.game.IndexGameService;
import com.example.backend.service.game.RegisterThrowService;
import java.util.List;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/game")
@AllArgsConstructor(access = AccessLevel.PRIVATE, onConstructor_ = @Autowired)
public class GameController
{
    private final CreateGameService createGameService;

    private final IndexGameService indexGameService;

    private final RegisterThrowService registerThrowService;

    @PostMapping
    @ResponseBody
    public Game create(@RequestBody final String name)
    {
        return createGameService.createGame(name);
    }

    @GetMapping
    @ResponseBody
    public List<Game> getAll()
    {
        return indexGameService.findAllGames();
    }

    @GetMapping("/{playerName}")
    @ResponseBody
    public Game get(@PathVariable("playerName") final String playerName)
    {
        return indexGameService.findGame(playerName);
    }

    @PostMapping("/{playerName}")
    @ResponseBody
    public String registerThrow(@PathVariable("playerName") final String playerName,
                                @RequestBody final int pins)
    {
        return registerThrowService.registerThrow(playerName, pins);
    }
}
