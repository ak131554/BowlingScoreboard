package com.example.backend;

import com.example.backend.entity.DataHolder;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendApplication
{
	private static final DataHolder DATA_HOLDER = new DataHolder();

	public static DataHolder getDataHolder()
	{
		return DATA_HOLDER;
	}

	public static void resetDataHolder()
	{
		DATA_HOLDER.reset();
	}

	public static void main(String[] args) {
		SpringApplication.run(BackendApplication.class, args);
	}
}
