package com.example.backend;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

public abstract class BackendTest
{
    @BeforeEach
    void setUp()
    {
        BackendApplication.resetDataHolder();
    }

    @AfterEach
    void tearDown()
    {
        BackendApplication.resetDataHolder();
    }
}
