package com.example.backend.controller;

import com.example.backend.BackendApplication;
import com.example.backend.BackendTest;
import com.example.backend.entity.DataHolder;
import com.example.backend.entity.Frame;
import com.example.backend.entity.Game;
import com.example.backend.entity.Player;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;

import java.util.List;
import java.util.Map;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GameControllerTest extends BackendTest
{
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;
    private String url;

    @BeforeEach
    void setup()
    {
        url = "http://localhost:" + port + "/game/";
    }

    @SuppressWarnings("unchecked")
    @Test
    void testWithExistingPlayer()
    {
        final String playerUrl = "http://localhost:" + port + "/player/";

        Assertions.assertEquals(0, restTemplate.getForObject(url, (Class<List<Game>>)(Class<?>) List.class).size());
        final Player john_doe = restTemplate.postForObject(playerUrl, "John Doe", Player.class);
        final Game game = restTemplate.postForObject(url, "John Doe", Game.class);
        Assertions.assertNotNull(game);
        Assertions.assertEquals(1, restTemplate.getForObject(url, (Class<List<Game>>)(Class<?>) List.class).size());
        Assertions.assertEquals(game, restTemplate.getForObject(url + "John Doe/", Game.class));
        Assertions.assertEquals(john_doe, game.getPlayer());
        Assertions.assertEquals(1, restTemplate.getForObject(playerUrl, (Class<List<Game>>)(Class<?>) List.class).size());
    }

    @SuppressWarnings("unchecked")
    @Test
    void testWithoutExistingPlayer()
    {
        final String playerUrl = "http://localhost:" + port + "/player/";

        Assertions.assertEquals(0, restTemplate.getForObject(url, (Class<List<Game>>)(Class<?>) List.class).size());
        final Game game = restTemplate.postForObject(url, "John Doe", Game.class);
        Assertions.assertNotNull(game);
        Assertions.assertEquals(1, restTemplate.getForObject(url, (Class<List<Game>>)(Class<?>) List.class).size());
        Assertions.assertEquals(game, restTemplate.getForObject(url + "John Doe/", Game.class));
        Assertions.assertEquals("John Doe", game.getPlayer().getName());
        Assertions.assertEquals(1, restTemplate.getForObject(playerUrl, (Class<List<Game>>)(Class<?>) List.class).size());
    }

    @Test
    void registerThrow()
    {
        String status = restTemplate.postForObject(url + "John Doe/", 5, String.class);
        Assertions.assertEquals("Game for Player John Doe not found", status);
        GameMock game = restTemplate.postForObject(url, "John Doe", GameMock.class);
        List<FrameMock> frames = game.frames();
        Assertions.assertEquals(10, frames.size());
        Assertions.assertEquals(frames.get(0), game.currentFrame());
        Assertions.assertNull(frames.get(0).score());
        Assertions.assertEquals(0, game.score());
        status = restTemplate.postForObject(url + "John Doe/", 5, String.class);
        Assertions.assertEquals("OK", status);
        game = restTemplate.getForObject(url + "John Doe/", GameMock.class);
        frames = game.frames();
        Assertions.assertEquals(frames.get(0), game.currentFrame());
        Assertions.assertNull(frames.get(0).score());
        Assertions.assertEquals(0, game.score());
        status = restTemplate.postForObject(url + "John Doe/", 2, String.class);
        Assertions.assertEquals("OK", status);
        game = restTemplate.getForObject(url + "John Doe/", GameMock.class);
        frames = game.frames();
        Assertions.assertEquals(frames.get(1), game.currentFrame());
        Assertions.assertEquals(7, frames.get(0).score());
        Assertions.assertNull(frames.get(1).score());
        Assertions.assertEquals(7, game.score());
        status = restTemplate.postForObject(url + "John Doe/", 10, String.class);
        Assertions.assertEquals("OK", status);
        game = restTemplate.getForObject(url + "John Doe/", GameMock.class);
        frames = game.frames();
        Assertions.assertEquals(frames.get(2), game.currentFrame());
        Assertions.assertEquals(7, frames.get(0).score());
        Assertions.assertNull(frames.get(1).score());
        Assertions.assertNull(frames.get(2).score());
        Assertions.assertEquals(7, game.score());
        status = restTemplate.postForObject(url + "John Doe/", 7, String.class);
        Assertions.assertEquals("OK", status);
        game = restTemplate.getForObject(url + "John Doe/", GameMock.class);
        frames = game.frames();
        Assertions.assertEquals(frames.get(2), game.currentFrame());
        Assertions.assertEquals(7, frames.get(0).score());
        Assertions.assertNull(frames.get(1).score());
        Assertions.assertNull(frames.get(2).score());
        Assertions.assertEquals(7, game.score());
        status = restTemplate.postForObject(url + "John Doe/", 4, String.class);
        Assertions.assertEquals("Sum of pins in current Frame would be greater than 10", status);
        game = restTemplate.getForObject(url + "John Doe/", GameMock.class);
        frames = game.frames();
        Assertions.assertEquals(frames.get(2), game.currentFrame());
        Assertions.assertEquals(7, frames.get(0).score());
        Assertions.assertNull(frames.get(1).score());
        Assertions.assertNull(frames.get(2).score());
        Assertions.assertEquals(7, game.score());
        status = restTemplate.postForObject(url + "John Doe/", 3, String.class);
        Assertions.assertEquals("OK", status);
        game = restTemplate.getForObject(url + "John Doe/", GameMock.class);
        frames = game.frames();
        Assertions.assertEquals(frames.get(3), game.currentFrame());
        Assertions.assertEquals(7, frames.get(0).score());
        Assertions.assertEquals(20, frames.get(1).score());
        Assertions.assertNull(frames.get(2).score());
        Assertions.assertNull(frames.get(3).score());
        Assertions.assertEquals(27, game.score());
        status = restTemplate.postForObject(url + "John Doe/", 0, String.class);
        Assertions.assertEquals("OK", status);
        game = restTemplate.getForObject(url + "John Doe/", GameMock.class);
        frames = game.frames();
        Assertions.assertEquals(frames.get(3), game.currentFrame());
        Assertions.assertEquals(7, frames.get(0).score());
        Assertions.assertEquals(20, frames.get(1).score());
        Assertions.assertEquals(10, frames.get(2).score());
        Assertions.assertNull(frames.get(3).score());
        Assertions.assertEquals(37, game.score());
        restTemplate.postForObject(url + "John Doe/", 10, String.class);
        restTemplate.postForObject(url + "John Doe/", 10, String.class);
        restTemplate.postForObject(url + "John Doe/", 10, String.class);
        restTemplate.postForObject(url + "John Doe/", 10, String.class);
        testLastFrameWithSpare();
        testLastFrameWithStrike();
        testLastFrameWithoutSpare();
    }

    private void testLastFrameWithSpare()
    {
        resetLastFrame();
        String status = restTemplate.postForObject(url + "John Doe/", 8, String.class);
        Assertions.assertEquals("OK", status);
        GameMock game = restTemplate.getForObject(url + "John Doe/", GameMock.class);
        List<FrameMock> frames = game.frames();
        Assertions.assertEquals(frames.get(9), game.currentFrame());
        Assertions.assertEquals(28, frames.get(7).score());
        Assertions.assertNull(frames.get(8).score());
        Assertions.assertNull(frames.get(9).score());
        Assertions.assertEquals(175, game.score());
        status = restTemplate.postForObject(url + "John Doe/", 5, String.class);
        Assertions.assertEquals("Sum of pins in current Frame would be greater than 10", status);
        game = restTemplate.getForObject(url + "John Doe/", GameMock.class);
        frames = game.frames();
        Assertions.assertEquals(frames.get(9), game.currentFrame());
        Assertions.assertNull(frames.get(8).score());
        Assertions.assertNull(frames.get(9).score());
        Assertions.assertEquals(175, game.score());
        status = restTemplate.postForObject(url + "John Doe/", 2, String.class);
        Assertions.assertEquals("OK", status);
        game = restTemplate.getForObject(url + "John Doe/", GameMock.class);
        frames = game.frames();
        Assertions.assertEquals(frames.get(9), game.currentFrame());
        Assertions.assertEquals(20, frames.get(8).score());
        Assertions.assertNull(frames.get(9).score());
        Assertions.assertEquals(195, game.score());
        status = restTemplate.postForObject(url + "John Doe/", 6, String.class);
        Assertions.assertEquals("OK", status);
        game = restTemplate.getForObject(url + "John Doe/", GameMock.class);
        frames = game.frames();
        Assertions.assertNull(game.currentFrame());
        Assertions.assertEquals(16, frames.get(9).score());
        Assertions.assertEquals(211, game.score());
        status = restTemplate.postForObject(url + "John Doe/", 6, String.class);
        Assertions.assertEquals("Game is already over", status);
    }

    private void testLastFrameWithStrike()
    {
        resetLastFrame();
        String status = restTemplate.postForObject(url + "John Doe/", 10, String.class);
        Assertions.assertEquals("OK", status);
        GameMock game = restTemplate.getForObject(url + "John Doe/", GameMock.class);
        List<FrameMock> frames = game.frames();
        Assertions.assertEquals(frames.get(9), game.currentFrame());
        Assertions.assertNull(frames.get(9).score());
        Assertions.assertEquals(177, game.score());
        status = restTemplate.postForObject(url + "John Doe/", 5, String.class);
        Assertions.assertEquals("OK", status);
        game = restTemplate.getForObject(url + "John Doe/", GameMock.class);
        frames = game.frames();
        Assertions.assertEquals(frames.get(9), game.currentFrame());
        Assertions.assertEquals(25, frames.get(8).score());
        Assertions.assertNull(frames.get(9).score());
        Assertions.assertEquals(202, game.score());
        status = restTemplate.postForObject(url + "John Doe/", 10, String.class);
        Assertions.assertEquals("OK", status);
        game = restTemplate.getForObject(url + "John Doe/", GameMock.class);
        frames = game.frames();
        Assertions.assertNull(game.currentFrame());
        Assertions.assertEquals(25, frames.get(9).score());
        Assertions.assertEquals(227, game.score());
        status = restTemplate.postForObject(url + "John Doe/", 6, String.class);
        Assertions.assertEquals("Game is already over", status);
    }

    private void testLastFrameWithoutSpare()
    {
        resetLastFrame();
        String status = restTemplate.postForObject(url + "John Doe/", 8, String.class);
        Assertions.assertEquals("OK", status);
        GameMock game = restTemplate.getForObject(url + "John Doe/", GameMock.class);
        List<FrameMock> frames = game.frames();
        Assertions.assertEquals(frames.get(9), game.currentFrame());
        Assertions.assertEquals(28, frames.get(7).score());
        Assertions.assertNull(frames.get(8).score());
        Assertions.assertNull(frames.get(9).score());
        Assertions.assertEquals(175, game.score());
        status = restTemplate.postForObject(url + "John Doe/", 1, String.class);
        Assertions.assertEquals("OK", status);
        game = restTemplate.getForObject(url + "John Doe/", GameMock.class);
        frames = game.frames();
        Assertions.assertNull(game.currentFrame());
        Assertions.assertEquals(19, frames.get(8).score());
        Assertions.assertEquals(9, frames.get(9).score());
        Assertions.assertEquals(203, game.score());
        status = restTemplate.postForObject(url + "John Doe/", 6, String.class);
        Assertions.assertEquals("Game is already over", status);
    }

    private void resetLastFrame()
    {
        final DataHolder dataHolder = BackendApplication.getDataHolder();
        final Player john_doe = dataHolder.getPlayers().get(0);
        Game game = dataHolder.getGames().get(john_doe);
        game.getFrames().set(7, new Frame(7));
        game.getFrames().set(8, new Frame(8));
        game.getFrames().set(9, new Frame(9));
        restTemplate.postForObject(url + "John Doe/", 10, String.class);
        String status = restTemplate.postForObject(url + "John Doe/", 10, String.class);
        Assertions.assertEquals("OK", status);
        final GameMock gameMock = restTemplate.getForObject(url + "John Doe/", GameMock.class);
        List<FrameMock> frames = gameMock.frames();
        Assertions.assertEquals(frames.get(9), gameMock.currentFrame());
        Assertions.assertNull(frames.get(7).score());
        Assertions.assertNull(frames.get(8).score());
        Assertions.assertNull(frames.get(9).score());
        Assertions.assertEquals(147, gameMock.score());
    }
}
