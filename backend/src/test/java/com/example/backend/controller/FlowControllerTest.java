package com.example.backend.controller;

import com.example.backend.BackendApplication;
import com.example.backend.BackendTest;
import com.example.backend.entity.Game;
import com.example.backend.entity.Player;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FlowControllerTest extends BackendTest
{
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;
    private String url;

    @BeforeEach
    void setup()
    {
        url = "http://localhost:" + port + "/flow/";
    }

    @Test
    void testStart()
    {
        String response = restTemplate.postForObject(url, "null", String.class);
        Assertions.assertEquals("No player defined", response);
        final Player john_doe = new Player("John Doe");
        BackendApplication.getDataHolder().getPlayers().add(john_doe);
        response = restTemplate.postForObject(url, "null", String.class);
        Assertions.assertEquals("No Games created for all players", response);
        BackendApplication.getDataHolder().getGames().put(john_doe, new Game(john_doe));
        response = restTemplate.postForObject(url, "null", String.class);
        Assertions.assertEquals("OK", response);
    }

    @Test
    void testNextTurnSinglePlayer()
    {
        final Player john_doe = new Player("John Doe");
        BackendApplication.getDataHolder().getPlayers().add(john_doe);
        final Game game = new Game(john_doe);
        BackendApplication.getDataHolder().getGames().put(john_doe, game);
        restTemplate.postForObject(url, "null", String.class);
        // State after Start
        StateMock state = restTemplate.getForObject(url, StateMock.class);
        Assertions.assertNotNull(state);
        Assertions.assertEquals(john_doe, state.currentPlayer());
        Assertions.assertEquals(1, state.numberOfFrame());
        Assertions.assertEquals(1, state.numberOfThrow());
        // No Strike
        game.registerThrow(5);
        state = restTemplate.getForObject(url, StateMock.class);
        Assertions.assertNotNull(state);
        Assertions.assertEquals(john_doe, state.currentPlayer());
        Assertions.assertEquals(1, state.numberOfFrame());
        Assertions.assertEquals(2, state.numberOfThrow());
        game.registerThrow(2);
        state = restTemplate.getForObject(url, StateMock.class);
        Assertions.assertNotNull(state);
        Assertions.assertEquals(john_doe, state.currentPlayer());
        Assertions.assertEquals(2, state.numberOfFrame());
        Assertions.assertEquals(1, state.numberOfThrow());
        // Spare
        game.registerThrow(7);
        state = restTemplate.getForObject(url, StateMock.class);
        Assertions.assertNotNull(state);
        Assertions.assertEquals(john_doe, state.currentPlayer());
        Assertions.assertEquals(2, state.numberOfFrame());
        Assertions.assertEquals(2, state.numberOfThrow());
        game.registerThrow(3);
        state = restTemplate.getForObject(url, StateMock.class);
        Assertions.assertNotNull(state);
        Assertions.assertEquals(john_doe, state.currentPlayer());
        Assertions.assertEquals(3, state.numberOfFrame());
        Assertions.assertEquals(1, state.numberOfThrow());
        // Strike
        game.registerThrow(10);
        state = restTemplate.getForObject(url, StateMock.class);
        Assertions.assertNotNull(state);
        Assertions.assertEquals(john_doe, state.currentPlayer());
        Assertions.assertEquals(4, state.numberOfFrame());
        Assertions.assertEquals(1, state.numberOfThrow());
        for (int i=3;i<9;i++)
        {
            game.registerThrow(10);
            state = restTemplate.getForObject(url, StateMock.class);
        }
        Assertions.assertEquals(john_doe, state.currentPlayer());
        Assertions.assertEquals(10, state.numberOfFrame());
        Assertions.assertEquals(1, state.numberOfThrow());
        game.registerThrow(10);
        state = restTemplate.getForObject(url, StateMock.class);
        Assertions.assertNotNull(state);
        Assertions.assertEquals(game.getPlayer(), state.currentPlayer());
        Assertions.assertEquals(10, state.numberOfFrame());
        Assertions.assertEquals(2, state.numberOfThrow());
        game.registerThrow(10);
        state = restTemplate.getForObject(url, StateMock.class);
        Assertions.assertNotNull(state);
        Assertions.assertEquals(game.getPlayer(), state.currentPlayer());
        Assertions.assertEquals(10, state.numberOfFrame());
        Assertions.assertEquals(3, state.numberOfThrow());
        game.registerThrow(10);
        state = restTemplate.getForObject(url, StateMock.class);
        Assertions.assertNull(state);
    }

    @Test
    void testNextTurnMultiPlayer()
    {
        final Player john_doe = new Player("John Doe");
        final Player max_muster = new Player("Max Muster");
        BackendApplication.getDataHolder().getPlayers().add(john_doe);
        BackendApplication.getDataHolder().getPlayers().add(max_muster);
        final Game game1 = new Game(john_doe);
        final Game game2 = new Game(max_muster);
        BackendApplication.getDataHolder().getGames().put(john_doe, game1);
        BackendApplication.getDataHolder().getGames().put(max_muster, game2);
        restTemplate.postForObject(url, "null", String.class);
        // State after Start
        StateMock state = restTemplate.getForObject(url, StateMock.class);
        Assertions.assertNotNull(state);
        Assertions.assertEquals(john_doe, state.currentPlayer());
        Assertions.assertEquals(1, state.numberOfFrame());
        Assertions.assertEquals(1, state.numberOfThrow());
        // No Strike
        game1.registerThrow(5);
        state = restTemplate.getForObject(url, StateMock.class);
        Assertions.assertNotNull(state);
        Assertions.assertEquals(john_doe, state.currentPlayer());
        Assertions.assertEquals(1, state.numberOfFrame());
        Assertions.assertEquals(2, state.numberOfThrow());
        game1.registerThrow(2);
        state = restTemplate.getForObject(url, StateMock.class);
        Assertions.assertNotNull(state);
        Assertions.assertEquals(max_muster, state.currentPlayer());
        Assertions.assertEquals(1, state.numberOfFrame());
        Assertions.assertEquals(1, state.numberOfThrow());
        game2.registerThrow(4);
        state = restTemplate.getForObject(url, StateMock.class);
        Assertions.assertNotNull(state);
        Assertions.assertEquals(max_muster, state.currentPlayer());
        Assertions.assertEquals(1, state.numberOfFrame());
        Assertions.assertEquals(2, state.numberOfThrow());
        game2.registerThrow(0);
        state = restTemplate.getForObject(url, StateMock.class);
        Assertions.assertNotNull(state);
        Assertions.assertEquals(john_doe, state.currentPlayer());
        Assertions.assertEquals(2, state.numberOfFrame());
        Assertions.assertEquals(1, state.numberOfThrow());
        // Spare
        game1.registerThrow(7);
        state = restTemplate.getForObject(url, StateMock.class);
        Assertions.assertNotNull(state);
        Assertions.assertEquals(john_doe, state.currentPlayer());
        Assertions.assertEquals(2, state.numberOfFrame());
        Assertions.assertEquals(2, state.numberOfThrow());
        game1.registerThrow(3);
        state = restTemplate.getForObject(url, StateMock.class);
        Assertions.assertNotNull(state);
        Assertions.assertEquals(max_muster, state.currentPlayer());
        Assertions.assertEquals(2, state.numberOfFrame());
        Assertions.assertEquals(1, state.numberOfThrow());
        game2.registerThrow(2);
        state = restTemplate.getForObject(url, StateMock.class);
        Assertions.assertNotNull(state);
        Assertions.assertEquals(max_muster, state.currentPlayer());
        Assertions.assertEquals(2, state.numberOfFrame());
        Assertions.assertEquals(2, state.numberOfThrow());
        game2.registerThrow(6);
        state = restTemplate.getForObject(url, StateMock.class);
        Assertions.assertNotNull(state);
        Assertions.assertEquals(john_doe, state.currentPlayer());
        Assertions.assertEquals(3, state.numberOfFrame());
        Assertions.assertEquals(1, state.numberOfThrow());
        game1.registerThrow(1);
        state = restTemplate.getForObject(url, StateMock.class);
        Assertions.assertNotNull(state);
        Assertions.assertEquals(john_doe, state.currentPlayer());
        Assertions.assertEquals(3, state.numberOfFrame());
        Assertions.assertEquals(2, state.numberOfThrow());
        game1.registerThrow(3);
        state = restTemplate.getForObject(url, StateMock.class);
        Assertions.assertNotNull(state);
        Assertions.assertEquals(max_muster, state.currentPlayer());
        Assertions.assertEquals(3, state.numberOfFrame());
        Assertions.assertEquals(1, state.numberOfThrow());
        game2.registerThrow(9);
        state = restTemplate.getForObject(url, StateMock.class);
        Assertions.assertNotNull(state);
        Assertions.assertEquals(max_muster, state.currentPlayer());
        Assertions.assertEquals(3, state.numberOfFrame());
        Assertions.assertEquals(2, state.numberOfThrow());
        game2.registerThrow(1);
        state = restTemplate.getForObject(url, StateMock.class);
        Assertions.assertNotNull(state);
        Assertions.assertEquals(john_doe, state.currentPlayer());
        Assertions.assertEquals(4, state.numberOfFrame());
        Assertions.assertEquals(1, state.numberOfThrow());
        // Strike
        game1.registerThrow(10);
        state = restTemplate.getForObject(url, StateMock.class);
        Assertions.assertNotNull(state);
        Assertions.assertEquals(max_muster, state.currentPlayer());
        Assertions.assertEquals(4, state.numberOfFrame());
        Assertions.assertEquals(1, state.numberOfThrow());
        game2.registerThrow(5);
        state = restTemplate.getForObject(url, StateMock.class);
        Assertions.assertNotNull(state);
        Assertions.assertEquals(max_muster, state.currentPlayer());
        Assertions.assertEquals(4, state.numberOfFrame());
        Assertions.assertEquals(2, state.numberOfThrow());
        game2.registerThrow(5);
        state = restTemplate.getForObject(url, StateMock.class);
        Assertions.assertNotNull(state);
        Assertions.assertEquals(john_doe, state.currentPlayer());
        Assertions.assertEquals(5, state.numberOfFrame());
        Assertions.assertEquals(1, state.numberOfThrow());
        game1.registerThrow(0);
        state = restTemplate.getForObject(url, StateMock.class);
        Assertions.assertNotNull(state);
        Assertions.assertEquals(john_doe, state.currentPlayer());
        Assertions.assertEquals(5, state.numberOfFrame());
        Assertions.assertEquals(2, state.numberOfThrow());
        game1.registerThrow(6);
        state = restTemplate.getForObject(url, StateMock.class);
        Assertions.assertNotNull(state);
        Assertions.assertEquals(max_muster, state.currentPlayer());
        Assertions.assertEquals(5, state.numberOfFrame());
        Assertions.assertEquals(1, state.numberOfThrow());
        game2.registerThrow(10);
        state = restTemplate.getForObject(url, StateMock.class);
        Assertions.assertNotNull(state);
        Assertions.assertEquals(john_doe, state.currentPlayer());
        Assertions.assertEquals(6, state.numberOfFrame());
        Assertions.assertEquals(1, state.numberOfThrow());
        for (int i=5;i<9;i++)
        {
            game1.registerThrow(10);
            restTemplate.getForObject(url, StateMock.class);
            game2.registerThrow(10);
            state = restTemplate.getForObject(url, StateMock.class);
        }
        Assertions.assertEquals(john_doe, state.currentPlayer());
        Assertions.assertEquals(10, state.numberOfFrame());
        Assertions.assertEquals(1, state.numberOfThrow());
        game1.registerThrow(10);
        state = restTemplate.getForObject(url, StateMock.class);
        Assertions.assertNotNull(state);
        Assertions.assertEquals(john_doe, state.currentPlayer());
        Assertions.assertEquals(10, state.numberOfFrame());
        Assertions.assertEquals(2, state.numberOfThrow());
        game1.registerThrow(10);
        state = restTemplate.getForObject(url, StateMock.class);
        Assertions.assertNotNull(state);
        Assertions.assertEquals(john_doe, state.currentPlayer());
        Assertions.assertEquals(10, state.numberOfFrame());
        Assertions.assertEquals(3, state.numberOfThrow());
        game1.registerThrow(10);
        state = restTemplate.getForObject(url, StateMock.class);
        Assertions.assertEquals(max_muster, state.currentPlayer());
        Assertions.assertEquals(10, state.numberOfFrame());
        Assertions.assertEquals(1, state.numberOfThrow());
        game2.registerThrow(2);
        state = restTemplate.getForObject(url, StateMock.class);
        Assertions.assertNotNull(state);
        Assertions.assertEquals(max_muster, state.currentPlayer());
        Assertions.assertEquals(10, state.numberOfFrame());
        Assertions.assertEquals(2, state.numberOfThrow());
        game2.registerThrow(2);
        state = restTemplate.getForObject(url, StateMock.class);
        Assertions.assertNull(state);
    }

    @Test
    void testReset()
    {
        final Player john_doe = new Player("John Doe");
        BackendApplication.getDataHolder().getPlayers().add(john_doe);
        final Game game = new Game(john_doe);
        BackendApplication.getDataHolder().getGames().put(john_doe, game);
        restTemplate.postForObject(url, "null", String.class);
        restTemplate.getForObject(url, StateMock.class);
        Assertions.assertNotNull(BackendApplication.getDataHolder());
        Assertions.assertFalse(BackendApplication.getDataHolder().getPlayers().isEmpty());
        Assertions.assertFalse(BackendApplication.getDataHolder().getGames().isEmpty());
        Assertions.assertFalse(BackendApplication.getDataHolder().getStateMachine().isReset());
        restTemplate.delete(url);
        Assertions.assertNotNull(BackendApplication.getDataHolder());
        Assertions.assertTrue(BackendApplication.getDataHolder().getPlayers().isEmpty());
        Assertions.assertTrue(BackendApplication.getDataHolder().getGames().isEmpty());
        Assertions.assertTrue(BackendApplication.getDataHolder().getStateMachine().isReset());
    }
}
