package com.example.backend.controller;

import com.example.backend.entity.Player;

public record StateMock(Player currentPlayer, int numberOfFrame, int numberOfThrow)
{
}
