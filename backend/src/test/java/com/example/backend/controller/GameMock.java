package com.example.backend.controller;

import java.util.List;

public record GameMock(List<FrameMock> frames, FrameMock currentFrame, Integer score)
{
}
