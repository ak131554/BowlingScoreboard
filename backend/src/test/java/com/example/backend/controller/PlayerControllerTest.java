package com.example.backend.controller;

import com.example.backend.BackendTest;
import com.example.backend.entity.Player;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;

import java.util.List;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PlayerControllerTest extends BackendTest
{
	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@SuppressWarnings("unchecked")
	@Test
	void testIt()
	{
		final String url = "http://localhost:" + port + "/player/";
		Assertions.assertEquals(0, restTemplate.getForObject(url, (Class<List<Player>>)(Class<?>) List.class).size());
		final Player john_doe = restTemplate.postForObject(url, "John Doe", Player.class);
		Assertions.assertNotNull(john_doe);
		Assertions.assertEquals("John Doe", john_doe.getName());
		Assertions.assertEquals(1, restTemplate.getForObject(url, (Class<List<Player>>)(Class<?>) List.class).size());
		Assertions.assertEquals(john_doe, restTemplate.getForObject(url + "0/", Player.class));
		final Player another_john_doe = restTemplate.postForObject(url, "John Doe", Player.class);
		Assertions.assertNotNull(another_john_doe);
		Assertions.assertEquals("John Doe", another_john_doe.getName());
		Assertions.assertEquals(1, restTemplate.getForObject(url, (Class<List<Player>>)(Class<?>) List.class).size());
		Assertions.assertEquals(john_doe, another_john_doe);
		final Player max_muster = restTemplate.postForObject(url, "Max Muster", Player.class);
		Assertions.assertNotNull(max_muster);
		Assertions.assertEquals("Max Muster", max_muster.getName());
		Assertions.assertEquals(2, restTemplate.getForObject(url, (Class<List<Player>>)(Class<?>) List.class).size());
		Assertions.assertEquals(max_muster, restTemplate.getForObject(url + "1/", Player.class));
		Assertions.assertNull(restTemplate.getForObject(url + "2/", Player.class));
	}
}
